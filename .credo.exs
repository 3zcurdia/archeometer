%{
  configs: [
    %{
      name: "default",
      files: %{
        excluded: [
          ~r"/_build/",
          ~r"/deps/",
          ~r"/node_modules/",
          ~r"/resources/"
        ]
      },
    }
  ]
}
