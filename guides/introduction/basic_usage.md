# Basic Usage

Since this is a tool about project discovery, first we have to collect all the data.

## Collect the data
To collect all the data simply run
```sh
mix arch.collect
```
This will run all sorts of different analysis and store all the data in a `archeometer_<your_project>.db` file.

You can skip running tests (for example, it tests take too long to run or they are broken) and collecting coverage information with the `--no-coverage` flag.
```sh
mix arch.collect --no-coverage
```

## Data collection stages
Under the hood, data collection is divided in several phases.

You can run each phase individually. They are
+ `arch.collect.static`: Analysis the AST of the project code. It requires [Credo](https://github.com/rrrene/credo).
+ `arch.collect.xrefs`: Collects information about module dependencies. It forces a compilation.
+ `arch.collect.apps`: Get information about the applications in the project. It must compile the project.
+ `arch.collect.ecto`: Collects modules with Ecto schema and their associations.
- `arch.collect.credo_issues`: to run Credo and collect the issues it found.
+ `arch.collect.coverage`: Calculate test coverage per module and function. It must run the test suite.

Depending on your project, you can choose to run only the stages you are interested in, or to skip some stage that is specially expensive to run.

The only necessary stage is `arch.collect.static` and it must be run before any other.

## Include dependencies in the analysis
Some collect tasks can also gather information from dependencies, they are
- static
- apps
- ecto
- xrefs

To filter the dependencies you can use the flag `--include-deps` and a unix-style glob pattern. The `credo_issues` tasks also accepts the flag but currently ignores it.

Pattern mechanisms:
- `*` matches none or many tokens
- `?` matches exactly one token
- `[abc]` matches a set of tokens
- `[a-z]` matches a range of tokens
- `[!...]` matches anything but a set of tokens

Example

```sh
mix arch.collect.static --include-deps 'phoenix*'
```

This will target:
- :phoenix
- :phoenix_ecto
- :phoenix_gon
- :phoenix_html
- :phoenix_live_dashboard
- :phoenix_live_reload
- :phoenix_live_view
- :phoenix_pubsub
- :phoenix_view
- ...

The flag `--include-deps` can be passed more than once, the dependencies set will be the union of all the filters, example.

```sh
mix arch.collect.static --include-deps 'ecto*' --include-deps 'archeometer'
```

This will target:
- :archeometer
- :ecto_sql
- :ecto

## Analysis Tasks
There are some predefined tasks to give you a brief overview of the project.

+ `mix arch.apps.xref`: Will create a dependency graph of the apps under the umbrella.
+ `mix arch.xref`: Will create a dependency graph in several possible formats.
+ `mix arch.dsm`: Will create a Design Structure Matrix, and will use it to find the cyclic dependencies in the project.
+ `mix arch.treemap`: Will generate a [treemap](https://en.wikipedia.org/wiki/Treemapping) of the modules within an specific application.
+ `mix arch.report`: Creates a comprehensive report on the project under review.

## Further reading
If you want to have a more in depth exploration of your project, don't forget to read the following guides:
+ [Queries and Schemas](../cql/queries_and_schemas.md): make queries to the data base using Elixir code
