defmodule Archeometer.Collect.XRef.Patch do
  @moduledoc """
  Compilation hooks to catch every reference between modules and store it in a
  ETS for latter processing.
  """

  def run() do
    Mix.shell().info("Starting cross reference analysis..")

    xrefs = Archeometer.Collect.XRef.compile_xrefs(tracer: __MODULE__, clear?: true)

    Mix.shell().info("Saving cross references...")

    Archeometer.Util.DumpStats.save_xrefs(xrefs)

    Mix.shell().info("Done!")
  end

  def trace({:require, _meta, module, _opts}, env),
    do: save_event({:require, module}, env)

  def trace({:struct_expansion, _meta, module, _keys}, env),
    do: save_event({:struct, module}, env)

  def trace({:remote_function, _meta, module, _function, _arity}, env),
    do: save_event({:remote_function, module}, env)

  def trace({:remote_macro, _meta, module, _function, _arity}, env),
    do: save_event({:remote_macro, module}, env)

  def trace({:imported_function, _meta, module, _function, _arity}, env),
    do: save_event({:imported_function, module}, env)

  def trace({:imported_macro, _meta, module, _function, _arity}, env),
    do: save_event({:imported_macro, module}, env)

  def trace(_event, _env),
    do: :ok

  def save_event({type, callee}, env) do
    dep = %{
      caller: env.module,
      callee: callee,
      type: type,
      file: env.file,
      line: env.line
    }

    :ets.insert(Archeometer.Collect.XRef, {env.module, dep})

    :ok
  end
end

Archeometer.Collect.XRef.Patch.run()
