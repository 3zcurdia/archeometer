/** @format */

const urlSearchParams = new URLSearchParams(window.location.search);

// Load and apply state of sidemenu
document.addEventListener("readystatechange", (event) => {
  urlSearchParams.forEach((value, key) =>
    collapseOrExpandSidemenuItem(value, "#" + key)
  );
  updateSidemenuLinks();
});

document.addEventListener("DOMContentLoaded", (event) => {
  configureSidemenuLists();
  configureScrollSections();
  configureCollapseSidemenu();
});

function copyCode(lang, uuid) {
  var copyText = document.getElementById(`${lang}-code-${uuid}`);
  copyText.select();
  copyText.setSelectionRange(0, 99999);
  navigator.clipboard.writeText(copyText.value);
  var text = document.querySelector(`#button-${lang}-code-${uuid} p`);
  text.innerHTML = "copied!";
}

function outCopy(codeId) {
  var text = document.querySelector(`#button-${codeId} p`);
  text.innerHTML = "copy";
}

function openLang(evt, lang, uuid) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName(`tabcontent-${uuid}`);
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName(`tablinks-${uuid}`);
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(`${lang}-tab-${uuid}`).style.display = "block";
  document
    .getElementById(`button-${lang}-code-${uuid}`)
    .classList.add(".d-none");
  evt.currentTarget.className += " active";
}

// Table super powers ⚡
$(document).ready(function () {
  $("table").DataTable({
    paging: false,
    order: [],
    dom: "Bfrtip",
    buttons: ["copy", "excel", "csv"],
    searching: false,
  });
});

// Scroll Sections //
function configureScrollSections() {
  const buttonSection = document.querySelector("div.dropdown-sections");
  const sections = document.querySelectorAll("main>.report-piece>h2");
  const sectionLinks = document.querySelectorAll(".dropdown-sections div a");
  const pageTitle = document.querySelector(".report-header>.title");
  const sidemenuLinks = document.querySelectorAll(
    `#sidelist-${pageTitle.innerHTML} li a span`
  );

  buttonSection.addEventListener("click", () => {
    buttonSection.classList.toggle("clicked");
  });

  const container = document.getElementById("report");

  container.addEventListener("scroll", () => {
    let currentSection = "";

    sections.forEach((section) => {
      const sectionTop = section.offsetTop;
      let containerWindow = container.scrollTop + container.clientHeight;
      if (
        (sectionTop >= container.scrollTop && sectionTop <= containerWindow) ||
        container.scrollTop >= sectionTop
      ) {
        currentSection = section.getAttribute("id");
      }
    });

    sectionLinks.forEach((sectionLink) => {
      sectionLink.classList.remove("active");
      if (sectionLink.innerHTML === currentSection) {
        sectionLink.classList.add("active");
      }
    });

    sidemenuLinks.forEach((sidemenuLink) => {
      sidemenuLink.classList.remove("active");
      if (sidemenuLink.innerHTML === currentSection) {
        sidemenuLink.classList.add("active");
      }
    });
  });
}

// Dropwdown Sections //
function dropdownSections() {
  const sectionsContainer = document.getElementById("sections-container");
  sectionsContainer.classList.toggle("d-none");
}

document.addEventListener("click", function (event) {
  const dropdownSectionsBtn = document.getElementById("dropdown-sections");

  const outsideClick = !dropdownSectionsBtn.contains(event.target);
  if (outsideClick) {
    const sectionsContainer = document.getElementById("sections-container");
    sectionsContainer.classList.add("d-none");
  }
});

/*** ZOOM & PANE SVG  ➕ ***/

function reset(event, uuid) {
  const svgImage = document.querySelector(`#svg-container-${uuid} > img`);
  svgImage.setAttribute("data-grabbed", 0);
  svgImage.setAttribute("data-x", 0);
  svgImage.setAttribute("data-y", 0);
  svgImage.style.left = "unset";
  svgImage.style.top = "unset";
  svgImage.style.transform = `scale(1)`;
  svgImage.setAttribute("data-scale", 1);
}

function isZoomPanEnable(uuid) {
  const svgContainer = document.getElementById(`svg-container-${uuid}`);
  const isZoomPanEnable = parseInt(svgContainer.getAttribute("data-zoompan"));
  return !!isZoomPanEnable;
}

function toogleZoomPanAvailability(event, uuid) {
  const svgContainer = document.getElementById(`svg-container-${uuid}`);
  const toggleButton = document.getElementById(`btn-zoom-pan-${uuid}`);
  const zoomInButton = document.getElementById(`zoom-in-${uuid}`);
  const zoomOutButton = document.getElementById(`zoom-out-${uuid}`);
  if (isZoomPanEnable(uuid)) {
    svgContainer.setAttribute("data-zoompan", 0);
    reset(event, uuid);
    toggleButton.children[1].classList.add("d-none");
    toggleButton.children[0].classList.remove("d-none");
    toggleButton.title = "Enable zoom and panning";
    zoomInButton.disabled = true;
    zoomOutButton.disabled = true;
    return;
  }
  svgContainer.setAttribute("data-zoompan", 1);
  toggleButton.children[0].classList.add("d-none");
  toggleButton.children[1].classList.remove("d-none");
  toggleButton.title = "Disable zoom and panning";
  zoomInButton.disabled = false;
  zoomOutButton.disabled = false;
}

/* ZOOM */
const STEP_SCALE = 0.05;
const MIN_SCALE = 0.05;
const MAX_SCALE = 100;

function getValidScale(scale, stepScale = STEP_SCALE) {
  let innerScale = scale;
  innerScale += stepScale;
  if (innerScale < MIN_SCALE) {
    innerScale = MIN_SCALE;
  }
  if (innerScale > MAX_SCALE) {
    innerScale = MAX_SCALE;
  }
  return Math.round(innerScale * 100) / 100;
}

function zoom(zoomFlow, scale, svgImage) {
  if (zoomFlow < 0) {
    scale = getValidScale(scale, STEP_SCALE);
  } else if (zoomFlow > 0) {
    scale = getValidScale(scale, -STEP_SCALE);
  }
  svgImage.style.transform = `scale(${scale})`;
  svgImage.setAttribute("data-scale", scale);
}

function handleZoom(event, uuid) {
  if (!isZoomPanEnable(uuid)) return;

  const svgImage = document.querySelector(`#svg-container-${uuid} > img`);
  const scale =
    Math.round(parseFloat(svgImage.getAttribute("data-scale")) * 100) / 100;
  zoom(event.deltaY, scale, svgImage, uuid);
  event.preventDefault();
  event.stopPropagation();
  event.stopImmediatePropagation();
  return false;
}

function zoomIn(event, uuid) {
  if (!isZoomPanEnable(uuid)) return;

  const svgImage = document.querySelector(`#svg-container-${uuid} > img`);
  const scale =
    Math.round(parseFloat(svgImage.getAttribute("data-scale")) * 100) / 100;
  zoom(-1, scale, svgImage, uuid);
}

function zoomOut(event, uuid) {
  if (!isZoomPanEnable(uuid)) return;
  const svgImage = document.querySelector(`#svg-container-${uuid} > img`);
  const scale =
    Math.round(parseFloat(svgImage.getAttribute("data-scale")) * 100) / 100;
  zoom(1, scale, svgImage, uuid);
}

/* PANNING */

function startPane(event, uuid) {
  if (!isZoomPanEnable(uuid)) return;
  const svgImage = document.querySelector(`#svg-container-${uuid} > img`);
  svgImage.setAttribute("data-grabbed", 1);
  svgImage.setAttribute("data-x", svgImage.offsetLeft - event.clientX);
  svgImage.setAttribute("data-y", svgImage.offsetTop - event.clientY);
}

function pane(event, uuid) {
  if (!isZoomPanEnable(uuid)) return;
  event.preventDefault();
  const svgImage = document.querySelector(`#svg-container-${uuid} > img`);
  const isGrabbed = parseInt(svgImage.getAttribute("data-grabbed"));
  if (isGrabbed) {
    const offsetX = parseFloat(svgImage.getAttribute("data-x"));
    const offsetY = parseFloat(svgImage.getAttribute("data-y"));
    svgImage.style.left = event.clientX + offsetX + "px";
    svgImage.style.top = event.clientY + offsetY + "px";
  }
}

function endPane(event, uuid) {
  if (!isZoomPanEnable(uuid)) return;
  const svgImage = document.querySelector(`#svg-container-${uuid} > img`);
  svgImage.setAttribute("data-grabbed", 0);
}

function openExternal(path) {
  window.open(path);
}

/** Sidmenu **/

// Collapsible menu list
function configureSidemenuLists() {
  const collapseButtons = document.getElementsByClassName("collapsible");
  for (let i = 0; i < collapseButtons.length; i++) {
    collapseButtons[i].addEventListener("click", function () {
      const content = this.parentElement.nextElementSibling;
      content.classList.toggle("d-none");
      this.classList.toggle("rotate-90");

      urlSearchParams.delete(this.parentElement.getAttribute("id"));
      if (this.classList.contains("rotate-90")) {
        urlSearchParams.append(this.parentElement.getAttribute("id"), "c");
      } else {
        urlSearchParams.append(this.parentElement.getAttribute("id"), "e");
      }
      updateSidemenuLinks();
    });
  }
}

function configureCollapseSidemenu() {
  const vertCollapseBtn = document.getElementById("nav-menu-collapse");
  vertCollapseBtn.addEventListener("click", function () {
    const sidemenu = document.getElementById("sidemenu");
    sidemenu.classList.toggle("collapsed-sidemenu");
    this.classList.toggle("rotate-180");
  });
}

function collapseOrExpandSidemenuItem(value, itemId) {
  const sidemenuItem = document.querySelector(itemId);

  if (sidemenuItem) {
    if (value === "e") {
      sidemenuItem.firstElementChild.classList.remove("rotate-90");
      sidemenuItem.nextElementSibling.classList.remove("d-none");
    } else if (value === "c") {
      sidemenuItem.firstElementChild.classList.add("rotate-90");
      sidemenuItem.nextElementSibling.classList.add("d-none");
    }
  }
}

function updateSidemenuLinks() {
  const sidemenuLinks = document.querySelectorAll(".sidemenu-item a");
  sidemenuLinks.forEach((link) => {
    const originalLink = link.getAttribute("href").split("#")[0].split("?")[0];
    const originalLinkSection = link
      .getAttribute("href")
      .split("#")
      .concat([""])[1];

    link.setAttribute(
      "href",
      originalLink +
        "?" +
        urlSearchParams.toString() +
        "#" +
        originalLinkSection
    );
  });
}
