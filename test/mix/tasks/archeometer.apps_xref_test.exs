defmodule Mix.Task.Arch.Apps.XRefTest do
  use ExUnit.Case

  @test_db "test/resources/db/archeometer_jissai.db"

  describe "successful invocation" do
    # All successful invocations require the --db parameter

    test "no args" do
      assert :ok = Mix.Task.rerun("arch.apps.xref", ["--db", @test_db])
    end

    test "--format" do
      assert :ok = Mix.Task.rerun("arch.apps.xref", ["--db", @test_db, "--format", "svg"])
    end

    test "--include-dep" do
      assert :ok = Mix.Task.rerun("arch.apps.xref", ["--db", @test_db, "--include-dep", "wild"])
    end
  end
end
