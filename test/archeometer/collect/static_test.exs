defmodule Archeometer.Collect.StaticTest do
  use ExUnit.Case
  alias Archeometer.Collect

  setup_all do
    inline_code = """
    defmodule TestModA do
      defstruct [:foo, :bar]
      def foo(), do: :ok
      def bar(arg), do: arg
      defp baz(arg) when is_atom(arg), do: :ok
      defp quux(), do: :this
    end
    """

    {:ok, ast: Code.string_to_quoted!(inline_code)}
  end

  test "process_module/2 returns a map with the module information", %{ast: ast} do
    module = Archeometer.Util.Code.collect_nodes(ast, :defmodule)

    assert Collect.Static.process_module(ast, module) ==
             %{
               name: "TestModA",
               num_lines: 6
             }
  end

  test "process_def/2 returns a map with the node information", %{ast: ast} do
    dfs = Archeometer.Util.Code.collect_nodes(ast, :def)

    assert Enum.map(dfs, &Collect.Static.process_def(ast, &1)) ==
             [
               {:ok,
                %{
                  args: [{:arg, [line: 4], nil}],
                  cc: 1,
                  module: "TestModA",
                  name: :bar,
                  num_lines: 1,
                  type: :def
                }},
               {:ok,
                %{
                  args: [],
                  cc: 1,
                  module: "TestModA",
                  name: :foo,
                  num_lines: 1,
                  type: :def
                }}
             ]
  end
end
