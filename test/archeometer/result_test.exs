defmodule Archeometer.Repo.ResultTest do
  use ExUnit.Case
  use Archeometer.SchemaFixtures
  import Archeometer.Query
  alias Archeometer.Schema.App

  doctest Archeometer.Repo.Result
  @test_db "test/resources/db/archeometer_jissai.db"

  test "struct query result to string" do
    query =
      from(a in App,
        select: [
          name: a.name,
          num_mods: count(a.modules.id)
        ],
        group_by: a.name,
        order_by: [desc: num_mods]
      )

    assert query |> Archeometer.Repo.all([], @test_db) |> to_string() ==
             """
              |name       |num_mods |
              |---------- |-------- |
              |jissai_web |61       |
              |jissai     |40       |
             """
  end
end
