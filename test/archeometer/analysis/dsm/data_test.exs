defmodule Archeometer.Analysis.DSM.DataTest do
  use ExUnit.Case

  alias Archeometer.Analysis.DSM.Data

  @test_db "test/resources/db/archeometer_jissai.db"
  @jissai_total_modules 101
  @jissai_non_test_modules 77

  describe "Data.get_data" do
    test "get all modules in project" do
      {modules, _} = Data.get_data(:none, "*", @test_db, false)

      assert Enum.count(modules) == @jissai_total_modules
    end

    test "get all modules except test modules" do
      {modules, _} = Data.get_data(:none, "*", @test_db, true)

      assert Enum.count(modules) == @jissai_non_test_modules

      any_tests =
        modules
        |> Map.values()
        |> Enum.any?(fn mod -> String.ends_with?(mod, "Test") end)

      refute any_tests
    end

    test "get data by modules namespace" do
      assert Data.get_data(:none, "Jissai.Repo", @test_db, true) ==
               {%{20 => "Jissai.Repo"}, %{20 => []}}
    end

    test "result empty for non existing namespace" do
      assert Data.get_data(:none, "Non.Existing", @test_db, true) == {%{}, %{}}
    end

    test "with app's name and no module's namespace" do
      {modules, _} = Data.get_data("jissai_web", "JissaiWeb", @test_db, true)

      only_jissai_web_modules =
        modules
        |> Map.values()
        |> Enum.all?(fn mod -> String.starts_with?(mod, "JissaiWeb") end)

      assert only_jissai_web_modules
    end

    test "with app name and module's namespace" do
      assert Data.get_data("jissai", "Jissai.Repo", @test_db, true) ==
               {%{20 => "Jissai.Repo"}, %{20 => []}}
    end
  end
end
