defmodule Archeometer.Util.DumpStatsTest do
  use ExUnit.Case
  use Archeometer.Repo
  alias Archeometer.Util.DumpStats
  alias Archeometer.DumpStatsFixtures

  @temp_db "temp_db.db"

  setup_all do
    DumpStats.save_modules(DumpStatsFixtures.modules_fixtures(), @temp_db)
    DumpStats.save_apps(DumpStatsFixtures.apps_fixtures(), @temp_db)
    on_exit(fn -> File.rm(@temp_db) end)
  end

  test "save_apps/2 saves a list of apps" do
    result =
      DumpStatsFixtures.apps_fixtures()
      |> DumpStats.save_apps(@temp_db)

    assert result == :ok
  end

  test "save_modules/2 saves a list of modules" do
    result =
      DumpStatsFixtures.modules_fixtures()
      |> DumpStats.save_modules(@temp_db)

    assert result == :ok
  end

  test "save_defs/3 saves a list of def nodes" do
    result =
      DumpStatsFixtures.defs_fixtures()
      |> DumpStats.save_defs("functions", @temp_db)

    assert result == :ok
  end

  test "add_module_coverages/2 adds the coverage of the listed modules" do
    result =
      DumpStatsFixtures.coverage_fixtures()
      |> DumpStats.add_module_coverages(@temp_db)

    assert result == :ok
  end

  test "add_modules_app_id/2 assigns the app_id to the listed modules" do
    result =
      DumpStatsFixtures.app_id_fixtures()
      |> DumpStats.add_modules_app_id(@temp_db)

    assert result == :ok
  end

  test "save_xref/2 saves the cross reference betwen two modules" do
    result =
      DumpStatsFixtures.xrefs_fixtures()
      |> DumpStats.save_xrefs(@temp_db)

    assert result == :ok
  end

  test "add_structs/2 assigns the structs to the listed modules" do
    result =
      DumpStatsFixtures.structs_fixtures()
      |> DumpStats.add_structs(@temp_db)

    assert result == :ok
  end

  test "save_behaviours/2 saves the listed behaviours" do
    result =
      DumpStatsFixtures.behaviours_fixtures()
      |> DumpStats.save_behaviours(@temp_db)

    assert result == :ok
  end

  test "save_credo_issues/2 successfuly saves behaviours" do
    assert :ok =
             DumpStatsFixtures.credo_issues_fixtures()
             |> DumpStats.save_credo_issues(@temp_db)
  end
end
