defmodule Archeometer.Util.CodeTest do
  use ExUnit.Case
  alias Archeometer.Util

  setup_all do
    inline_code = """
    defmodule TestModA do
      defstruct [:foo, :bar]
      def foo(), do: :ok
      def bar(arg), do: arg
      defp baz(arg) when is_atom(arg), do: :ok
      defp quux(), do: :this
      def boo() do
        def = :hello
      end
    end
    """

    {:ok, ast: Code.string_to_quoted!(inline_code)}
  end

  test "collect_nodes/2 returns the requested types of nodes", %{ast: ast} do
    ast
    |> Util.Code.collect_nodes(:def)
    |> Enum.each(fn node ->
      assert {:def, _, _} = node
    end)

    ast
    |> Util.Code.collect_nodes([:defp, :defstruct])
    |> Enum.each(fn node ->
      correct_type =
        case node do
          {:defp, _, _} -> true
          {:defstruct, _, _} -> true
          _ -> false
        end

      assert correct_type
    end)
  end

  test "get_meta/2 returns the metadata of the given AST", %{ast: ast} do
    assert Util.Code.get_meta(ast, :line) == 1
  end

  test "get_decl/2 returns declaration information from the AST def-like macro", %{ast: ast} do
    ast
    |> Util.Code.collect_nodes(:def)
    |> Enum.each(fn
      {:def, _, nil} = node ->
        assert {:error, _} = Util.Code.get_decl(node)

      {:def, _, _} = node ->
        assert {:ok, _} = Util.Code.get_decl(node)
    end)
  end

  test "resolve_mod_name/2 returns the module name of the given AST", %{ast: ast} do
    df = Util.Code.collect_nodes(ast, :def) |> List.first()
    assert Util.Code.resolve_mod_name(ast, df) == "TestModA"
  end

  test "count_lines/1 returns the number of lines of the ast", %{ast: ast} do
    assert Util.Code.num_lines(ast) == 8
  end
end
