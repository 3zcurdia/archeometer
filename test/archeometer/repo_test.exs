defmodule Archeometer.RepoTest do
  use ExUnit.Case
  use Archeometer.SchemaFixtures

  alias Archeometer.Schema.Module
  alias Archeometer.Repo.Result
  import Archeometer.Query

  @test_db "test/resources/db/archeometer_jissai.db"

  @main_app "jissai"

  describe "get all" do
    @test_queries [
      raw: {"SELECT m.name FROM modules m;", nil},
      simple: {from(m in Module, select: m.name), ["m.name"]},
      multi: {
        from(m in Module, select: {m.name, m.num_lines}),
        ["m.name", "m.num_lines"]
      },
      aggregated: {
        from(m in Module, select: {m.name, avg(m.functions.cc)}),
        ["m.name", "avg(m.functions.cc)"]
      },
      named: {
        from(m in Module, select: %{n: m.name, fun_cc: m.functions.cc}),
        [:n, :fun_cc]
      },
      interpolated: {
        from(
          m in Module,
          select: m.name,
          where: m.app.name == ^@main_app,
          where: like(m.name, ^String.capitalize("jissai.%"))
        ),
        ["m.name"]
      }
    ]

    for {kind, {query, header}} <- @test_queries do
      @query query
      @headers header
      test "#{kind} query get a correct result" do
        assert %Result{headers: headers, rows: rows} =
                 Archeometer.Repo.all(
                   @query,
                   [],
                   @test_db
                 )

        refute Enum.empty?(rows)
        assert @headers == headers
      end
    end
  end

  test "DB ready?" do
    assert Archeometer.Repo.db_ready?(:full, @test_db)
  end

  test "Execute r aw" do
    alias Exqlite.Sqlite3, as: DB

    query = "UPDATE modules SET app_id = ? WHERE name = ?2"

    {_, conn} = DB.open(@test_db)
    result = Archeometer.Repo.execute_raw(conn, query, [1, "Jissai"])
    DB.close(conn)
    assert :ok = result
  end
end
