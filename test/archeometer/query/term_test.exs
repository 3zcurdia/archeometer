defmodule Archeometer.Query.TermTest do
  use ExUnit.Case

  alias Archeometer.Query.Term

  describe "term validation" do
    @valid_literals [
      integer: 1,
      float: 1.0,
      bool: true,
      bool: false,
      string: "foo"
    ]

    for {type, lit} <- @valid_literals do
      test "#{type} literal `#{lit}` is valid" do
        assert {:ok, unquote(lit)} = Term.validate(unquote(lit))
      end
    end

    @valid_lists [
      char: 'foo',
      hom: [0, 2, 3],
      het: [0, true, "foo"]
    ]

    for {type, lit} <- @valid_lists do
      test "#{type} list `#{Macro.to_string(lit)}` is valid" do
        assert {:ok, {:list, _, unquote(lit)}} = Term.validate(unquote(lit))
      end
    end

    @invalid_literals [
      atom: :foo,
      atom: Foo
    ]

    for {type, lit} <- @invalid_literals do
      test "#{type} literal `#{lit}` is invalid" do
        assert {:error, _} = Term.validate(unquote(lit))
      end
    end

    Module.put_attribute(
      __MODULE__,
      :valid_escapes,
      quote do
        [
          escape: ^0,
          escape: ^10
        ]
      end
    )

    for {kind, ast} <- @valid_escapes do
      test "#{kind} fragment `#{Macro.to_string(ast)}` is valid" do
        assert {:ok, {:^, [], _}} = Term.validate(unquote(Macro.escape(ast)))
      end
    end

    Module.put_attribute(
      __MODULE__,
      :valid_builtins,
      quote do
        [
          prefix_unary: is_nil(1),
          prefix_unary:
            unquote(
              quote do
                not true
              end
            ),
          prefix_unary: avg(not is_nil(1)),
          prefix_unary: count(avg(not is_nil(1))),
          prefix_unary: max(count(avg(not is_nil(1)))),
          prefix_unary: min(max(count(avg(not is_nil(1))))),
          prefix_unary: sum(min(max(count(avg(not is_nil(1)))))),
          prefix_unary: exists(1),
          prefix_unary: length(1),
          prefix_binary: like(1, 2),
          infix_binary: sum(1) * min(2),
          infix_binary: not is_nil(1) and false,
          infix_binary: 2 * max(5) / max(3 * 3),
          infix_binary: 5 / 4 * avg(6) + avg(4 * 3),
          infix_binary: 4 + 5 * min(4) / 7 - (4 + 9),
          infix_binary: 1 == 0 - 0,
          infix_binary: avg(7) * 4 != 3,
          infix_binary: min(2) < 1 - 8 * count(0),
          infix_binary: 8 + 5 - 2 > max(4) / 5,
          infix_binary: sum(0) <= avg(4),
          infix_binary: 0 in [0, 2, 3, 4],
          infix_binary: is_nil(5) >= count(6) or exists(2),
          infix_binary: is_nil(5) >= count(6) and 8 + 5 - 2 > max(4) / 5,
          infix_binary: sum(0) <= avg(4) or is_nil(5) >= count(6),
          nary_func: replace(0, 1, 2)
        ]
      end
    )

    for {kind, ast} <- @valid_builtins do
      test "#{kind} builtin `#{Macro.to_string(ast)}` is valid" do
        assert {:ok, {_, [], [_ | _]}} = Term.validate(unquote(Macro.escape(ast)))
      end
    end

    Module.put_attribute(
      __MODULE__,
      :invalid_builtins,
      quote do
        [
          wrong_num_args: like,
          wrong_num_args: avg(),
          wrong_num_args: like(1),
          wrong_num_args: count(1, 2),
          wrong_num_args: max(1, 2, 3),
          wrong_num_args: min(1, 2, 3, 4),
          wrong_num_args: sum(1, 2, 3, 4, 5)
        ]
      end
    )

    for {kind, ast} <- @invalid_builtins do
      test "builint with #{kind} `#{Macro.to_string(ast)}` is invalid" do
        assert {:error, _} = Term.validate(unquote(Macro.escape(ast)))
      end
    end

    Module.put_attribute(
      __MODULE__,
      :valid_symbols,
      quote do
        [
          lookup: modules,
          lookup: macros.num_lines,
          lookup: modules.functions.cc,
          lookup: function.module,
          expr: modules.lines + functions.cc,
          expr: like(modules.name, "Elixir.%"),
          expr: is_nil(module.app),
          expr: not (functions.cc > 10),
          expr: functions.cc > 10,
          expr: modules.name in ["Elixir"],
          expr: sum(macros.cc) / count(macros.id),
          expr: exists(macros.name) and not exists(functions.name),
          expr: max(functions.num_args) != min(macros.num_args),
          expr: max(num_lines) != min(macros.num_args) and functions.cc > 10,
          expr: max(num_lines) <= avg(functions.num_args) or macros.cc > 5
        ]
      end
    )

    for {kind, ast} <- @valid_symbols do
      test "symbol in #{kind} `#{Macro.to_string(ast)}` is valid" do
        assert {:ok, _} = Term.validate(unquote(Macro.escape(ast)))
      end
    end
  end

  describe "term serialization" do
    Module.put_attribute(
      __MODULE__,
      :serialized_terms,
      quote do
        [
          integer: {1, "1"},
          float: {1.0, "1.0"},
          bool: {true, "true"},
          bool: {false, "false"},
          string: {"foo", "'foo'"},
          func: {length(""), "length('')"},
          func: {replace("", "a", "b"), "replace('','a','b')"},
          prefix_binary: {like(1, 2), "(1 LIKE 2)"},
          infix_binary: {4 + 5 * min(4) / 7 - (4 + 9), "((4 + ((5 * min(4)) / 7)) - (4 + 9))"},
          infix_binary: {1 == 0 - 0, "(1 == (0 - 0))"},
          infix_binary: {not is_nil(5) >= count(6), "(5 NOTNULL >= count(6))"},
          infix_binary: {module.name in ["OTP", "BEAM"], "(module.name in ('OTP','BEAM'))"},
          infix_binary: {like(4, 5) or is_nil(5), "((4 LIKE 5) OR 5 ISNULL)"},
          expr: {modules.lines + functions.cc, "(modules.lines + functions.cc)"},
          expr: {like(modules.name, "Elixir.%"), "(modules.name LIKE 'Elixir.%\')"},
          expr: {is_nil(module.app), "module.app ISNULL"},
          expr: {exists("SELECT 0"), "EXISTS (SELECT 0)"},
          expr: {not (functions.cc > 10), "NOT (functions.cc > 10)"},
          list: {["Elixir", "OTP", "Erlang"], "('Elixir','OTP','Erlang')"}
        ]
      end
    )

    for {key, {ast, expected}} <- @serialized_terms do
      test "#{key} `#{Macro.to_string(ast)}` maps to `#{expected}`" do
        {:ok, valid} = Term.validate(unquote(Macro.escape(ast)))

        assert {:ok, io_lit} = Term.to_iodata(valid)
        assert unquote(expected) == IO.iodata_to_binary(io_lit)
      end
    end

    Module.put_attribute(
      __MODULE__,
      :invalid_serialized_terms,
      quote do
        [
          expr: 5 * not_here(true, "foo"),
          expr: true and avg(not_here(), false),
          expr: avg(not_here(1), false)
        ]
      end
    )

    for {key, ast} <- @invalid_serialized_terms do
      test "#{key} `#{Macro.to_string(ast)}` fails" do
        assert {:error, error} = Term.to_iodata(unquote(Macro.escape(ast)))
        assert {:invalid_operator, _} = error
      end
    end
  end

  describe "term to ast" do
    defp assert_equiv({op0, _meta, module0}, {op1, _meta1, module1})
         when is_atom(module0) and is_atom(module1) do
      assert op0 == op1
    end

    defp assert_equiv({op0, _meta0, args0}, {op1, _meta1, args1}) do
      assert_equiv(op0, op1)

      Enum.zip(args0, args1)
      |> Enum.each(fn {arg0, arg1} ->
        assert_equiv(arg0, arg1)
      end)
    end

    defp assert_equiv(ast0, ast1), do: assert(ast0 == ast1)

    Module.put_attribute(
      __MODULE__,
      :valid_symbols,
      quote do
        [
          escaped: ^42,
          lookup: modules,
          lookup: macros.num_lines,
          lookup: modules.functions.cc,
          lookup: function.module,
          expr: modules.lines + functions.cc,
          expr: like(modules.name, "Elixir.%"),
          expr: is_nil(module.app) and true,
          expr: not (functions.cc > 10),
          expr: a in [],
          expr: functions.cc > 5.09,
          expr: sum(macros.cc) / count(macros.id),
          expr: max(functions.num_args) != min(macros.num_args),
          expr: max(num_lines) != min(macros.num_args) and functions.cc > 10,
          expr: max(num_lines) <= avg(functions.num_args) or macros.cc > 5,
          list: ["Elixir"]
        ]
      end
    )

    for {kind, ast} <- @valid_symbols do
      test "#{kind} `#{Macro.to_string(ast)}` validation can be reverted" do
        a = unquote(Macro.escape(ast))
        new_ast = Term.validate(a) |> elem(1) |> Term.to_ast()
        assert_equiv(new_ast, a)
      end
    end
  end
end
