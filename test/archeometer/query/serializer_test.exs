defmodule Archeometer.Query.SerializerTest do
  use ExUnit.Case
  use Archeometer.SchemaFixtures

  alias Archeometer.Query.Serializer

  import Archeometer.Query

  doctest Archeometer.Query.Serializer

  describe "query serialization" do
    test "erroneous queries are propagated" do
      assert {:error, _} = Serializer.to_sql(from(m in ModuleA, select: m.not_here))
      assert {:error, _} = Serializer.serialize_query(from(m in ModuleA, select: m.not_here))

      assert {:error, _} =
               Serializer.serialize_query(
                 {:ok, %{source: A, select: A, bindings: [], tables: nil}}
               )
    end

    @simple_queries [
      simple_selection: from(m in ModuleA, select: avg(m.id), where: m.id in [0, 2, 3]),
      named_selection: from(m in ModuleA, select: %{i: m.id * 2}),
      multiple_selection: from(m in ModuleA, select: [i: m.id - 2, j: m.id / 10]),
      with_where: from(m in ModuleA, select: %{id: m.id}, where: like(id, "foo")),
      with_order: from(m in ModuleA, select: m.id, order_by: {count(m.id)}),
      with_order_having:
        from(m in ModuleA, select: sum(m.id), order_by: m.id, having: is_nil(m.id))
    ]

    for {kind, query} <- @simple_queries do
      @q query
      test "for #{kind} works" do
        {:ok, _} = Serializer.serialize_query(@q)
      end
    end

    @multitable_queries [
      foreign_selection:
        from(m in ModuleA, select: m.module_b.id, where: m.module_b.id in [0, 2, 3]),
      foreign_named_selection: from(m in ModuleA, select: [i: m.module_b.id]),
      foreign_multiple_selection:
        from(m in ModuleA, select: %{i: m.module_b.id, j: m.module_b.module_c.id}),
      foreign_with_order: from(m in ModuleA, select: m.id, order_by: {m.module_b.id}),
      foreign_with_order_having:
        from(m in ModuleA, select: m.id, order_by: m.module_b.id, having: m.module_b.id > 1),
      subqueries: from(m in ModuleA, select: m.id, where: exists(m.module_b.module_c.id))
    ]

    for {kind, query} <- @multitable_queries do
      @q query
      test "for #{kind} works" do
        {:ok, _} = Serializer.serialize_query(@q)
      end
    end
  end
end
