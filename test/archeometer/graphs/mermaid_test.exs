defmodule Archeometer.Graph.MermaidTest do
  use ExUnit.Case
  alias Archeometer.Graphs.Mermaid
  doctest Archeometer.Graphs.Mermaid, import: true

  @non_cyclic %{
    1 => [],
    2 => [1, 3],
    3 => [1],
    4 => [2, 3]
  }

  @cyclic %{
    1 => [4],
    2 => [1],
    3 => [1, 2],
    4 => [2]
  }

  describe "Mermaid " do
    test "with non cyclic reference" do
      mermaid_str = Mermaid.render(@non_cyclic)

      assert String.starts_with?(mermaid_str, "graph TD;")
      assert String.ends_with?(mermaid_str, ";\n")

      lines = String.split(mermaid_str, "\n")

      # For modules

      Enum.each(@non_cyclic |> Map.keys() |> Enum.with_index(), fn {module, id} ->
        assert is_module_declared?(lines, module, id)
      end)

      # For relationships

      Enum.each(@non_cyclic, fn {origin, dests} ->
        Enum.each(dests, fn dest ->
          assert is_reference_declared?(lines, origin, dest)
        end)
      end)
    end

    test "with cyclic reference" do
      mermaid_str = Mermaid.render(@cyclic)

      assert String.starts_with?(mermaid_str, "graph TD;")
      assert String.ends_with?(mermaid_str, ";\n")

      lines = String.split(mermaid_str, "\n")

      # For modules

      Enum.each(@cyclic |> Map.keys() |> Enum.with_index(), fn {module, id} ->
        assert is_module_declared?(lines, module, id)
      end)

      # For relationships

      Enum.each(@cyclic, fn {origin, dests} ->
        Enum.each(dests, fn dest ->
          assert is_reference_declared?(lines, origin, dest)
        end)
      end)
    end
  end

  defp is_module_declared?(lines, module, id) do
    {:ok, rx} = Regex.compile("^[[:space:]]+id_#{id}\\(\\[#{module}\\]\\)")
    Enum.count(lines, fn line -> Regex.match?(rx, line) end) == 1
  end

  defp is_reference_declared?(lines, from, to) do
    {:ok, rx} = Regex.compile("^[[:space:]]+id_#{from - 1}-->id_#{to - 1}")
    Enum.count(lines, fn line -> Regex.match?(rx, line) end) == 1
  end
end
