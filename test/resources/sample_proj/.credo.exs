%{
  configs: [
    %{
      name: "default",
      files: %{
        included: [
          "lib/"
        ],
        excluded: [~r"/_build/"]
      }
    }
  ]
}
