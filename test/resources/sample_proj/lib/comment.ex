defmodule Sample.Comment do
  @moduledoc false
  use Ecto.Schema

  schema "comments" do
    field(:body, :string)
    timestamps()
  end
end
