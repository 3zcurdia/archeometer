defmodule SampleProj.PostComment do
  @moduledoc false
  use Ecto.Schema

  schema "post_comments" do
    belongs_to(:post, SampleProj.Post)
    belongs_to(:comment, SampleProj.Comment)
  end
end
