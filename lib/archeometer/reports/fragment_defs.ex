defmodule Archeometer.Reports.FragmentDefs do
  @moduledoc false

  alias Archeometer.Reports.Fragment

  defmodule Application do
    @moduledoc false

    def applications_and_modules() do
      %Fragment.Definition{
        query_type: :cql,
        result_type: :table,
        desc: "List of applications and their module count",
        result_name: :none,
        table_headers: ["name", "num_mods", "num_lines", "num_ecto_schemas"],
        code: """
        Repo.all(
          from a in App,
            select: [
              name: a.name,
              num_mods: count(a.modules.id),
              num_lines: sum(a.modules.num_lines),
              num_ecto_schemas: sum(a.modules.has_ecto_schema)
            ],
            group_by: a.name,
            order_by: [desc: num_mods]
        )
        """,
        alt_code: """
        select a.name, count(m.id) num_mods, sum(m.num_lines) num_lines, sum(m.has_ecto_schema) num_ecto_schemas
        from apps a
        inner join modules m on a.id = m.app_id
        group by a.name
        order by num_mods desc;
        """,
        alt_code_lang: :sql
      }
    end

    def applications_xrefs() do
      %Fragment.Definition{
        query_type: :mix_task,
        result_type: :svg,
        desc: "Dependency graph between applications",
        result_name: :none,
        code: "mix arch.apps.xref --format svg --out <%= @fname %>"
      }
    end
  end

  defmodule Size do
    @moduledoc false
    def largest_modules() do
      %Fragment.Definition{
        query_type: :cql,
        result_type: :table,
        desc: "Top <%= @limit %> largest modules, measured by LoC (Lines of Code).",
        result_name: :none,
        table_headers: ["name", "num_lines"],
        code: """
        Repo.all(
          from m in Module,
            select: [
              name: m.name,
              num_lines: m.num_lines
            ],
            order_by: [desc: num_lines],
            where: m.app.name == "<%= @app %>",
            limit: <%= @limit %>
        )
        """,
        alt_code: """
        select m.name, m.num_lines
        from modules m
        inner join apps a on m.app_id = a.id
        where a.name = '<%= @app %>'
        order by m.num_lines desc
        limit <%= @limit %>;
        """,
        alt_code_lang: :sql
      }
    end

    def module_size_treemap() do
      %Fragment.Definition{
        query_type: :mix_task,
        result_type: :treemap_svg,
        desc:
          "This treemap represents a hierarchical decomposition of modules. The area of each module representing its relative size.",
        result_name: :none,
        code: "mix arch.treemap --metric size --app <%= @app %> --out <%= @fname %>"
      }
    end
  end

  defmodule Complexity do
    @moduledoc false

    def most_complex_modules() do
      %Fragment.Definition{
        query_type: :cql,
        result_type: :table,
        desc: "Top <%= @limit %> modules with the most complexity.",
        result_name: :none,
        table_headers: ["name", "aggregated_cc", "average_cc"],
        code: """
        Repo.all(
          from m in Module,
            select: [
              name: m.name,
              aggregated_cc: sum(m.functions.cc),
              average_cc: round(sum(m.functions.cc) * 1.0 / count(m.functions.id), 2)
            ],
            group_by: m.name,
            order_by: [desc: average_cc],
            where: m.app.name == "<%= @app %>",
            limit: <%= @limit %>
        )
        """,
        alt_code: """
        select m.name,
             sum(f.cc)  aggregated_cc,
             round(sum(f.cc) * 1.0 / count(f.id), 2) average_cc
        from modules m
        inner join functions f
        on m.id = f.module_id
        inner join apps a
        on m.app_id = a.id
        where a.name = '<%= @app %>'
        group by m.name
        order by average_cc desc
        limit <%= @limit %>;
        """,
        alt_code_lang: :sql
      }
    end

    def most_complex_functions() do
      %Fragment.Definition{
        query_type: :cql,
        result_type: :table,
        desc: "Top <%= @limit %> functions with the most complexity.",
        result_name: :none,
        table_headers: ["module_name", "fun_name", "fun_arity", "fun_cc"],
        code: """
        Repo.all(
          from f in Function,
            select: [
              module_name: f.module.name,
              fun_name: f.name,
              fun_arity: f.num_args,
              fun_cc: f.cc
            ],
            order_by: [desc: fun_cc],
            where: f.module.app.name == "<%= @app %>",
            limit: <%= @limit %>
        )
        """,
        alt_code: """
        select m.name module_name, f.name fun_name, f.num_args fun_arity, f.cc fun_cc
        from functions f
        inner join modules m
        on f.module_id = m.id
        inner join apps a
        on m.app_id = a.id
        where a.name = '<%= @app %>'
        order by fun_cc desc
        limit <%= @limit %>;
        """,
        alt_code_lang: :sql
      }
    end
  end

  defmodule Core do
    @moduledoc false

    def modules_with_most_deps() do
      %Fragment.Definition{
        query_type: :cql,
        result_type: :table,
        desc: "Modules with the most dependencies.",
        result_name: :none,
        table_headers: ["name", "num_callees"],
        code: """
        Repo.all(
          from x in XRef,
            select: [
              name: x.caller.name,
              num_callees: count(x.callee.name)
            ],
            group_by: x.caller.id,
            order_by: [desc: num_callees],
            where: x.caller.app.name == "<%= @app %>",
            limit: <%= @limit %>
        )
        """,
        alt_code: """
        select m.name, count(x.callee_id) num_callees
        from xrefs x
        inner join modules m
        on x.caller_id = m.id
        inner join apps a
        on a.id = m.app_id
        where a.name = '<%= @app %>'
        group by x.caller_id
        order by num_callees desc
        limit <%= @limit %>;
        """,
        alt_code_lang: :sql
      }
    end

    def core_modules() do
      %Fragment.Definition{
        query_type: :cql,
        result_type: :table,
        desc:
          "Possibly the intersection between the biggest modules and those with the most dependencies gives us a hint about the core modules.",
        result_name: :mod_names,
        table_headers: ["name"],
        code: """
        biggest_modules =
          Repo.all(
            from m in Module,
              select: [
                name: m.name
              ],
              order_by: [desc: m.num_lines],
              where: m.app.name == "<%= @app %>",
              limit: <%= @limit %>
          )

          most_dependencies_modules =
            Repo.all(
              from x in XRef,
                select: [
                  name: x.caller.name
                ],
                group_by: x.caller.id,
                order_by: [desc: count(x.callee.name)],
                where: x.caller.app.name == "<%= @app %>",
                limit: <%= @limit %>
            )

          intersection(biggest_modules, most_dependencies_modules)
        """,
        alt_code: """
        select name from (
          select m.name, m.num_lines
          from modules m
          inner join apps a
          on m.app_id = a.id
          where a.name = '<%= @app %>'
          order by num_lines desc
          limit <%= @limit %>
          )
        intersect
        select name from (
          select m.name, count(x.callee_id) num_callees
          from xrefs x
          inner join modules m
          on x.caller_id = m.id
          inner join apps a
          on a.id = m.app_id
          where a.name = '<%= @app %>'
          group by x.caller_id
          order by num_callees desc
          limit <%= @limit %>
          )
        """,
        alt_code_lang: :sql
      }
    end

    def core_modules_graph() do
      %Fragment.Definition{
        query_type: :mix_task,
        receives: :mod_names,
        result_type: :svg,
        desc: "This is de dependency graph between the (hypothetically) core modules.",
        result_name: :none,
        code: "mix arch.xref --format svg --out <%= @fname %> <%= @mod_names %>"
      }
    end
  end

  defmodule BuildingBlocks do
    @moduledoc false

    def most_used_modules() do
      %Fragment.Definition{
        query_type: :cql,
        result_type: :table,
        desc: "Modules most used within the application.",
        result_name: :none,
        table_headers: ["name", "num_callers"],
        code: """
        Repo.all(
          from x in XRef,
            select: [
              name: x.callee.name,
              num_callers: count(x.caller.name)
            ],
            group_by: x.callee.name,
            order_by: [desc: num_callers],
            where: x.callee.app.name == "<%= @app %>",
            limit: <%= @limit %>
        )
        """,
        alt_code: """
        select m.name, count(x.caller_id) num_callers
        from xrefs x inner join modules m on x.callee_id = m.id
        inner join apps a on m.app_id = a.id
        where a.name = '<%= @app %>'
        group by m.name
        order by num_callers
        desc limit <%= @limit %>;
        """,
        alt_code_lang: :sql
      }
    end
  end

  defmodule API do
    @moduledoc false

    def modules_with_most_public_funs() do
      %Fragment.Definition{
        query_type: :cql,
        result_type: :table,
        desc: "Modules with the most public functions.",
        result_name: :none,
        table_headers: ["name", "num_funs"],
        code: """
        Repo.all(
          from f in Function,
            select: [
              name: f.module.name,
              num_funs: count(f.id)
            ],
            where: f.module.app.name == "<%= @app %>" and f.type == "def",
            group_by: f.module.id,
            order_by: [desc: num_funs],
            limit: <%= @limit %>
        )
        """,
        alt_code: """
        select m.name, count(f.id) as num_funs
        from functions f
        inner join modules m on m.id = f.module_id
        inner join apps a on m.app_id = a.id
        where a.name = '<%= @app %>' and f.type = 'def'
        group by f.module_id
        order by num_funs desc
        limit <%= @limit %>;
        """,
        alt_code_lang: :sql
      }
    end
  end

  defmodule Ecto do
    @moduledoc false

    def ecto_schemas() do
      %Fragment.Definition{
        query_type: :cql,
        result_type: :table,
        result_name: :ecto_schemas,
        table_headers: ["name"],
        desc: "These are the Ecto schemas defined in the application.",
        code: """
        Repo.all(
          from m in Module,
            where: m.has_ecto_schema and m.app.name == "<%= @app %>",
            select: [name: m.name],
            order_by: [asc: name]
        )
        """,
        alt_code: """
        select m.name
        from modules m
        inner join apps a on m.app_id = a.id
        where m.has_ecto_schema and a.name = '<%= @app %>';
        """,
        alt_code_lang: :sql
      }
    end

    def ecto_dependency_graph() do
      %Fragment.Definition{
        query_type: :mix_task,
        result_type: :svg,
        receives: :ecto_schemas,
        desc:
          "This is the dependency graph of the Ecto schemas. (If detail is too small, you can right-click and open it in other tab).",
        result_name: :none,
        code: "mix arch.xref --format svg --out <%= @fname %> <%= @ecto_schemas %>"
      }
    end
  end

  defmodule OTP do
    @moduledoc false

    def otp_behaviours() do
      %Fragment.Definition{
        query_type: :cql,
        result_type: :table,
        result_name: :none,
        table_headers: ["otp_behaviour", "module"],
        desc: "These are the modules implementing OTP behaviours.",
        code: """
        Repo.all(
          from b in Behaviour,
            select: [
              otp_behaviour: b.name,
              module: b.module.name
            ],
            where:
              b.name in ["Application", "Agent", "GenServer", "GenEvent", "Supervisor"],
            where: b.module.app.name == "<%= @app %>",
            order_by: otp_behaviour
        )
        """,
        alt_code: """
        select b.name otp_behaviour, m.name module
        from behaviours b
        inner join modules m on b.module_id = m.id
        inner join apps a on m.app_id == a.id
        where
          a.name == '<%= @app %>'
          and otp_behaviour in ('Application', 'Agent', 'GenServer', 'GenEvent', 'Supervisor')
        order by otp_behaviour asc;
        """,
        alt_code_lang: :sql
      }
    end
  end

  defmodule DepsAnalysis do
    @moduledoc false

    def dsm_matrix() do
      %Fragment.Definition{
        query_type: :mix_task,
        result_type: :image,
        desc:
          "Colored squares respresent groups of modules with cyclic dependencies between them (open image in another tab if it is too small).",
        result_name: :none,
        code: "mix arch.dsm --app <%= @app %> --format svg --out <%= @fname %>"
      }
    end

    def dsm_largest_deps_group() do
      %Fragment.Definition{
        query_type: :elixir,
        result_type: :table,
        desc:
          "To get the largest group of cyclic dependencies, you could use the following code.",
        table_headers: ["name"],
        result_name: :mod_names,
        code: """
        DSM.largest_cyclic_deps_group("<%= @app %>", "<%= @db_name %>")
        |> Enum.map(&[&1])
        """
      }
    end

    def dsm_largest_deps_group_graph() do
      %Fragment.Definition{
        query_type: :mix_task,
        result_type: :svg,
        desc: "You can visualize the dependencies between the afore mentioned modules.",
        result_name: :none,
        receives: :mod_names,
        code: "mix arch.xref --format svg --out <%= @fname %> <%= @mod_names %>"
      }
    end

    def deps_graph() do
      fdef = Core.core_modules_graph()
      %{fdef | desc: "Cyclic dependency example"}
    end
  end

  defmodule TestCoverage do
    @moduledoc false

    def modules_with_least_coverage() do
      %Fragment.Definition{
        query_type: :cql,
        result_type: :table,
        desc: "Modules with the least coverage.",
        result_name: :none,
        table_headers: ["name", "num_lines", "coverage", "uncovered_lines"],
        code: """
        Repo.all(
          from m in Module,
            select: [
              name: m.name,
              num_lines: m.num_lines,
              coverage: round(m.coverage, 2),
              uncovered_lines: round(m.num_lines * (1 - m.coverage), 0)
            ],
            order_by: [desc: uncovered_lines],
            where: m.app.name == "<%= @app %>" and m.coverage < 0.5,
            limit: <%= @limit %>
        )
        """,
        alt_code: """
        select m.name,
             num_lines,
             round(m.coverage, 2) coverage,
             round(num_lines * (1 - coverage), 0) as uncovered_lines
        from modules m
        inner join apps a on m.app_id = a.id
        where a.name = '<%= @app %>' and m.coverage < 0.5
        order by uncovered_lines desc
        limit <%= @limit %>
        """,
        alt_code_lang: :sql
      }
    end
  end
end
