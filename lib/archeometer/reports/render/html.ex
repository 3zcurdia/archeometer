defmodule Archeometer.Reports.Render.Html do
  @moduledoc """
  Renders a page into HTML.
  """
  require EEx

  EEx.function_from_file(
    :def,
    :render,
    "priv/templates/report/html/base_report.eex",
    [
      :app_data,
      :page_names,
      :renderer
    ]
  )

  EEx.function_from_file(:def, :main_report, "priv/templates/report/html/main_report.eex", [
    :app_data,
    :renderer
  ])

  EEx.function_from_file(
    :def,
    :nav_side_menu,
    "priv/templates/report/html/nav_side_menu.eex",
    [
      :page_names,
      :renderer
    ]
  )

  EEx.function_from_file(:def, :header, "priv/templates/report/html/header.eex", [
    :app_data,
    :renderer
  ])

  EEx.function_from_file(:def, :sections, "priv/templates/report/html/sections.eex", [
    :sections,
    :_renderer
  ])

  EEx.function_from_file(:def, :fragment, "priv/templates/report/html/fragment.eex", [
    :fragment,
    :renderer
  ])

  EEx.function_from_file(
    :def,
    :code_fragment,
    "priv/templates/report/html/code_fragment.eex",
    [
      :fragment,
      :renderer
    ]
  )

  EEx.function_from_file(:def, :button_tab, "priv/templates/report/html/button_tab.eex", [
    :fragment,
    :type,
    :_renderer
  ])

  EEx.function_from_file(:def, :tab_content, "priv/templates/report/html/tab_content.eex", [
    :code_lang,
    :uuid,
    :code_fragment,
    :renderer
  ])

  EEx.function_from_file(:def, :button_copy, "priv/templates/report/html/button_copy.eex", [
    :uuid,
    :code_lang,
    :_renderer
  ])

  EEx.function_from_file(:def, :result_table, "priv/templates/report/html/result_table.eex", [
    :fragment,
    :_renderer
  ])

  EEx.function_from_file(
    :def,
    :fragment_result,
    "priv/templates/report/html/fragment_result.eex",
    [
      :fragment,
      :renderer
    ]
  )

  EEx.function_from_file(
    :def,
    :svg_container,
    "priv/templates/report/html/svg_container.eex",
    [
      :fragment_uuid,
      :svg_path,
      :_renderer
    ]
  )

  EEx.function_from_file(
    :def,
    :sidemenu_item,
    "priv/templates/report/html/sidemenu_item.eex",
    [
      :item,
      :_renderer
    ]
  )
end
