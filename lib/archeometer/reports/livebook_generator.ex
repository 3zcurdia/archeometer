defmodule Archeometer.Reports.LiveBookGenerator do
  @moduledoc """
  Generates a report given a configuration and parameters.
  """

  use Archeometer.Repo
  import Archeometer.Reports.Utils
  import Archeometer.Util.PathHelper
  require Logger
  require EEx

  # alias Archeometer.Reports.Page

  def generate(cfg) do
    Logger.info("Preparing report paths...")
    prepare_paths(:livemd)
    Logger.info("Creating pages...")
    create_pages(cfg)
    # Logger.info("Copying assets...")
    # copy_assets()
    # Logger.info("Copying images...")
    # copy_images()
    Logger.info("Report generated!")
  end

  @index_page_name :index

  defp create_pages(cfg) do
    case length(cfg.modules) do
      1 ->
        create_module_page(hd(cfg.modules), cfg)

      _ ->
        page_names = page_names_fn(cfg)

        create_main_page(cfg, page_names)

        Enum.each(cfg.modules, &create_module_page(&1, cfg, page_names))
    end
  end

  defp page_names_fn(cfg) do
    sections =
      Enum.map(
        cfg.modules,
        &module_section_names(&1, cfg.module_page_def)
      )

    mods = Enum.zip(cfg.modules, sections)
    index_name = to_string(@index_page_name)

    index = {
      index_name,
      module_section_names(index_name, cfg.root_page_def)
    }

    [index | mods]
  end

  defp module_section_names(mod, page_def) do
    page_def.(mod)
    |> Map.get(:sections)
    |> Enum.map(&Map.get(&1, :desc))
  end

  defp create_main_page(cfg, _page_names) do
    Logger.info("Creating page: main ...")
    file_name = to_string(@index_page_name)

    cfg.db_name
    |> absolute_path()
    |> render_main()
    |> write_to_file(:livemd, file_name <> ".livemd")

    Logger.info("Page main ready!")
  end

  defp create_module_page(module, cfg, _page_names \\ []) do
    Logger.info("Creating page: #{module} ...")

    db = absolute_path(cfg.db_name)

    module
    |> render_module(db)
    |> write_to_file(:livemd, module <> ".livemd")

    Logger.info("Page #{module} ready!")
  end

  EEx.function_from_file(
    :defp,
    :render_main,
    "priv/templates/report/livemd/index.livemd.eex",
    [
      :db
    ]
  )

  EEx.function_from_file(
    :defp,
    :render_module,
    "priv/templates/report/livemd/application.livemd.eex",
    [
      :app,
      :db
    ]
  )
end
