defmodule Archeometer.Reports.PageDefinition.Project do
  @moduledoc """
  Defines the page structure for a project.
  """

  alias Archeometer.Reports.Page
  alias Archeometer.Reports.Section
  alias Archeometer.Reports.FragmentDefs

  def definition(name) do
    %Page.Definition{
      id: name,
      sections: [
        %Section.Definition{
          id: :applications,
          desc: "Project Structure",
          fragments: [
            FragmentDefs.Application.applications_and_modules(),
            FragmentDefs.Application.applications_xrefs()
          ]
        }
      ]
    }
  end
end
