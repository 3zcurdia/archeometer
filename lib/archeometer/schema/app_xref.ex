defmodule Archeometer.Schema.AppXRef do
  @moduledoc """
  Represents an cross reference between applications.

  The schema has the following fields:
  - `caller` is a reference to `Archeometer.Schema.App`. The module were the
  referece took place.
  - `callee` is a reference to `Archeometer.Schema.App`. The module being
  referenced.
  """

  use Archeometer.Schema

  alias Archeometer.Schema.App

  defschema(:apps_xrefs) do
    belongs_to(App, as: :caller, primary_key: true)
    belongs_to(App, as: :callee, primary_key: true)
  end
end
