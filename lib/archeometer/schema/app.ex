defmodule Archeometer.Schema.App do
  @moduledoc """
  This modules represents a OTP application. It holds the relevant schema data
  that will be used in the project analysis.

  This schema has the following fields:
  - `name` is the name of the OTP application
  - `is_external` if the application is external (dependency)
  - `modules` is a reference to `Archeometer.Schema.Modules`. All the modules
  belonging to the application.
  """

  use Archeometer.Schema

  alias Archeometer.Schema.{Module, AppXRef}

  defschema :apps do
    field(:id, primary_key: true)
    field(:name)
    field(:is_external)
    has(Module, as: :modules, key: :app_id)
    has(AppXRef, as: :in_refs, key: :callee_id)
    has(AppXRef, as: :out_refs, key: :caller_id)
  end
end
