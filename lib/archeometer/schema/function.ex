defmodule Archeometer.Schema.Function do
  @moduledoc """
  Represents an Elixir function. Holds the relevant schema data that will be
  used in the project analysis.

  This schema has the following fields:
  - `name` is the name of the function
  - `cc` represents the cyclomatic complexity.
  - `num_lines` is the number of lines in the function definition.
  - `num_args` is the number of arguments the function receives.
  - `arg_names` is a comma separated list with the name of the parameters the
  function receives.
  - `type` can be either `def` or `defp` depending on the function declaration.
  - `coverage` is a number between 0 and 1 representing the percentage of tested
  lines of code. It is pulled directly from Erlang's `code` module, so it might
  not be very accurate.
  - `module` is a referece to `Archeometer.Schema.Module`. It is the module
  where the function was declared.
  """

  use Archeometer.Schema

  alias Archeometer.Schema.Module

  defschema :functions do
    field(:id, primary_key: true)
    field(:name)
    field(:cc)
    field(:num_lines)
    field(:num_args)
    field(:arg_names)
    field(:type)
    field(:coverage)
    belongs_to(Module)
  end
end
