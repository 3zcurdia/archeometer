defmodule Archeometer.Schema.CredoIssue do
  use Archeometer.Schema

  @moduledoc """
  Represents a credo issue found in a module.

  - `check_module` is the name of the failed check.
  - `line_no` the line number where the issue was found.
  - `severity` the is the importance of the failed check.
  - `module` is a reference to `Archeometer.Schema.Module`.
    It is the module were the issue was found
  """

  alias Archeometer.Schema.Module

  defschema :credo_issues do
    field(:id, primary_key: true)
    field(:check_module)
    field(:line_no)
    field(:severty)
    belongs_to(Module)
  end
end
