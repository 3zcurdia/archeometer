defmodule Archeometer.Schema.XRef do
  @moduledoc """
  Represents an cross reference between modules. It contains the relevant schema
  data that will be used in the project analysis.

  The schema has the following fields:
  - `type` is the corresponding to the cross reference. The full list of events
  is documented on the `Code` module.
  - `line` is the line number in the file where the reference was detected.
  - `caller` is a reference to `Archeometer.Schema.Module`. The module were the
  referece took place.
  - `callee` is a reference to `Archeometer.Schema.Module`. The module being
  referenced.
  """

  use Archeometer.Schema

  alias Archeometer.Schema.Module

  defschema(:xrefs) do
    field(:type)
    field(:line)
    belongs_to(Module, as: :caller, primary_key: true)
    belongs_to(Module, as: :callee, primary_key: true)
  end
end
