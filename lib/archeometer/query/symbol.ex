defmodule Archeometer.Query.Symbol do
  @moduledoc false

  alias Archeometer.Query.Term

  @doc """
  A symbol in the DSL is a table lookup. After being syntactically validated
  with `Archeometer.Query.Term`, the symbols stil don't know their tables.

  This function provides the context resolution for symbols by using the
  underling `Archeometer.Schema`.
  """
  def resolve_symbols(ast, alias_table)

  def resolve_symbols({:symb, _meta, [{arg, _, nil}]}, alias_table) do
    case Map.get(alias_table, arg) do
      nil ->
        {:error, {:top_level_non_alias, arg}}

      {:table_alias, _other_meta} ->
        {:error, {:table_not_selectable, arg}}

      {:expr_alias, expr} ->
        {:ok, expr}
    end
  end

  def resolve_symbols({:symb, meta0, [{symb, symb_meta, nil} | next]}, alias_table) do
    case Map.get(alias_table, symb) do
      nil ->
        {:error, {:top_level_non_alias, symb}}

      {:table_alias, table_meta} ->
        resolve_symbol(next, Keyword.get(table_meta, :dest))
        |> ok_do(fn resolved ->
          {:symb, meta0, [{symb, symb_meta ++ table_meta, nil} | resolved]}
        end)

      {:expr_alias, expr} ->
        {:error, {:non_table_prefix, {symb, expr}}}
    end
  end

  # For now we only analyze the `select` field of a subquery. If we support more
  # subquery operators in the future we will need a more flexible solution.
  def resolve_symbols(%{select: [[sel]]} = subquery, alias_table) do
    resolve_symbols(sel, alias_table)
    |> ok_do(fn res -> %{subquery | select: [[res]]} end)
  end

  def resolve_symbols({op, meta, args}, alias_table) when is_list(args) do
    args
    |> Enum.reduce(
      {:ok, {op, meta, []}},
      &resolve_operator_symbols(&1, &2, alias_table)
    )
    |> ok_do(fn {op, meta, resolved_args} ->
      {op, meta, Enum.reverse(resolved_args)}
    end)
  end

  def resolve_symbols(expr, _alias_table), do: Term.validate(expr)

  defp resolve_symbol([{arg, meta0, nil}], source) do
    with {:ok, meta1} <- generate_assoc_meta(arg, source) do
      if Keyword.has_key?(meta1, :key?) do
        {:error, {:table_not_selectable, arg}}
      else
        {:ok, [{arg, meta0 ++ meta1, nil}]}
      end
    end
  end

  defp resolve_symbol([{symb, meta0, nil} | next], source) do
    with {:ok, assoc_meta} <- generate_assoc_meta(symb, source),
         {:ok, next_lookup} <-
           resolve_symbol(next, get_next_source(assoc_meta)) do
      if Keyword.has_key?(assoc_meta, :field?) do
        {:error, {:non_table_prefix, symb}}
      else
        {:ok, [{symb, meta0 ++ assoc_meta, nil} | next_lookup]}
      end
    end
  end

  defp resolve_operator_symbols(arg, {:ok, {op, meta, prev_args}}, alias_table) do
    resolve_symbols(arg, alias_table)
    |> ok_do(fn resolved -> {op, meta, [resolved | prev_args]} end)
  end

  defp resolve_operator_symbols(_arg, {:error, _} = error, _aliases), do: error

  defp ok_do({:ok, elem}, fun), do: {:ok, fun.(elem)}
  defp ok_do({:error, _} = error, _fun), do: error

  defp get_next_source(meta),
    do: if(meta[:field?], do: meta[:source], else: meta[:dest])

  defp generate_assoc_meta(arg, source) do
    cond do
      arg in source.__archeometer_fields__() ->
        {:ok, [source: source, field?: true]}

      Map.has_key?(source.__archeometer_assocs__(), arg) ->
        assoc = source.__archeometer_assocs__()[arg]

        keys = generate_assoc_keys(assoc, source)
        {:ok, [key?: true, source: source, dest: assoc.module, on: keys]}

      true ->
        {:error, {:unresolved_symb, {arg, source}}}
    end
  end

  defp generate_assoc_keys(assoc, source) do
    case assoc.type do
      :local ->
        {assoc.keys, assoc.module.__archeometer_keys__()}

      :remote ->
        {source.__archeometer_keys__(), assoc.keys}
    end
  end

  @doc """
  Replace all flattened symbols with a symbol `table.field` lookup, using the
  given alias table.

  That is, replace the whole prefix of a lookup with the alias corresponding to
  the joined table.
  """
  def replace_aliases(expr, alias_table)

  def replace_aliases({:symb, meta, args}, alias_table) when is_list(args) do
    {last, rest} = List.pop_at(args, -1)
    prefix = Enum.map_join(rest, ".", &elem(&1, 0))

    case Map.get(alias_table, prefix) do
      %{dest: %{alias: a}} ->
        {:ok, {:symb, meta, [a, last]}}

      _ ->
        {:error, {:symb_without_table, args}}
    end
  end

  def replace_aliases({op, meta, args}, alias_table) when is_list(args) do
    Enum.reduce(args, {:ok, {op, meta, []}}, fn arg, {:ok, {o, m, prevs}} ->
      arg
      |> replace_aliases(alias_table)
      |> ok_do(fn rep_arg -> {o, m, [rep_arg | prevs]} end)
    end)
    |> ok_do(fn {op, meta, rep_args} -> {op, meta, Enum.reverse(rep_args)} end)
  end

  # If a subquery arrived here, it is during the first pass of the serialization.
  # Subqueries are recursively serialized, so there is no need to replace their
  # aliases during the first pass.
  def replace_aliases(%{} = subquery, _alias_table), do: {:ok, subquery}

  def replace_aliases(other, _alias_table), do: Term.validate(other)
end
