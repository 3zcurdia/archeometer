defmodule Archeometer.Util.PathHelper do
  @moduledoc false

  def template_path(template) do
    Path.expand(template, template_path())
  end

  def template_path() do
    Application.app_dir(:archeometer, "priv/templates")
  end

  def absolute_path(path) do
    File.cwd!() |> Path.join(path)
  end
end
