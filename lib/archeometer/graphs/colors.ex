defmodule Archeometer.Graphs.Colors do
  @moduledoc false

  @fill_color "#D3D3FF"
  @line_color "#9470db"
  @color_palette_base "#A0A0FF" |> CssColors.parse!()
  @lightening_increments [0.00, 0.05, 0.10, 0.15, 0.20]
  @color_palette @lightening_increments
                 |> Enum.map(fn inc ->
                   CssColors.lighten(@color_palette_base, inc) |> CssColors.rgb() |> to_string()
                 end)
  @color_ranges Enum.zip([:l5, :l4, :l3, :l2, :l1], @color_palette) |> Map.new()

  def fill_color(), do: @fill_color

  def line_color(), do: @line_color

  @doc """
  Retrieves a color from the defined palette. 'x' must be a float value between [0, 1].
  """
  def color_for(x) when is_float(x) do
    color_range =
      cond do
        x == 0 -> :l1
        x > 0 and x <= 0.25 -> :l2
        x > 0.25 and x <= 0.5 -> :l3
        x > 0.5 and x <= 0.75 -> :l4
        x > 0.75 -> :l5
      end

    Map.get(@color_ranges, color_range)
  end
end
