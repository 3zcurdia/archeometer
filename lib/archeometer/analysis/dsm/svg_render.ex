defmodule Archeometer.Analysis.DSM.SVGRender do
  @moduledoc """
  Functions to render an `Archeometer.Analysis.DSM` struct into a
  [svg](https://www.w3schools.com/graphics/svg_intro.asp) image.
  """
  require EEx
  alias Archeometer.Analysis.DSM

  EEx.function_from_file(
    :defp,
    :render_svg,
    "priv/templates/svg/dsm_svg.eex",
    [:mtx, :mod_names, :group_corners]
  )

  @doc """
  Renders a `DSM` struct into a `svg`.

  Returns the string representing the generated `svg` image.

  The resulting `svg` can be written to a file or directly embedded into an `HTML`.

  ## Parameters

  - `dsm`. The `DSM` to be rendered.
  - `mod_names`. A map from module ids to their corresponding names.

  """
  def render(%DSM{} = dsm, mod_names) do
    group_corners = DSM.Utils.upper_right_group_corners(dsm)

    render_svg(dsm, mod_names, group_corners)
    |> String.replace(~r/\n\s+\n/, "\n")
  end
end
