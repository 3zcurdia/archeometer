defmodule Archeometer.Collect.Credo.SaveStatsTask do
  @moduledoc """
  Credo Task to collect and save metrics from an static analysis of an Elixir project..
  """

  use Credo.Execution.Task
  alias Credo.CLI.Output.UI
  alias Archeometer.Util.DumpStats
  alias Archeometer.Collect.Static

  def call(exec, test_paths) do
    UI.puts("Listing modules...")

    mods =
      exec
      |> Execution.get_source_files()
      |> Static.module_stats_for(test_paths)

    UI.puts("Listing defs...")

    funcs =
      exec
      |> Execution.get_source_files()
      |> Static.def_stats_for([:def, :defp])

    UI.puts("Listing macros...")

    macros =
      exec
      |> Execution.get_source_files()
      |> Static.def_stats_for([:defmacro, :defmacrop])

    UI.puts("\nAnalysis done!\n")

    UI.puts("Saving modules...")
    DumpStats.save_modules(mods)

    UI.puts("Saving functions...")
    DumpStats.save_defs(funcs, "functions")

    UI.puts("Saving macros...")
    DumpStats.save_defs(macros, "macros")

    UI.puts([:green, "Done!"])

    exec
  end
end
