defmodule Archeometer.Collect.CredoIssues do
  @moduledoc """
  Module for running Credo into the current project and collect
  all the issues it found.
  """

  defstruct [:check, :module, :line_no, :severity]

  def new(%Credo.Issue{} = issue) do
    struct(__MODULE__, Map.from_struct(issue))
    |> Map.put(:module, issue_mod(issue))
  end

  defp issue_mod(%Credo.Issue{} = issue) do
    issue
    |> Map.get(:scope, "")
    |> Credo.Code.Scope.mod_name()
    |> flip_concat("Elixir.")
    |> String.to_atom()
  end

  defp flip_concat(a, b), do: b <> a

  def collect(collection \\ %{}) do
    Credo.Application.start(nil, nil)

    issues =
      Credo.run([])
      |> Credo.Execution.get_issues()
      |> Enum.map(&new/1)

    Map.put(collection, :credo_issues, issues)
  end
end
