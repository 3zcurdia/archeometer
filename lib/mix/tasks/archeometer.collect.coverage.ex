defmodule Mix.Tasks.Arch.Collect.Coverage do
  @moduledoc """
  Mix Task to collect and store test coverage information into a Archeometer database.
  """
  use Mix.Task
  alias Archeometer.Collect.Coverage
  alias Archeometer.Util.DumpStats

  @shortdoc "Dumps test coverage of current project into Archeometer DB"

  @preferred_cli_env :test

  @impl Mix.Task
  def run(_argv) do
    # Otherwise Repo.db_ready? would fail misserably
    Application.ensure_started(:archeometer)

    if Archeometer.Repo.db_ready?(:basic) do
      Mix.shell().info("Starting test coverage analysis...")

      Mix.shell().info("Collecting coverage information...")
      _code_server = Coverage.prepare_code_server()

      Mix.shell().info("Calculating test coverage per module..")
      mod_coverage = Coverage.calculate_module_coverage()

      Mix.shell().info("Calculating test coverage per function..")
      fun_coverage = Coverage.calculate_function_coverage()

      Mix.shell().info("Coverage calculated! Now saving values to the database")
      DumpStats.add_module_coverages(mod_coverage)
      DumpStats.add_function_coverages(fun_coverage)

      Mix.shell().info("All values saved to the database!")
    else
      Mix.shell().error("Please run static analysis first")
      {:error, :no_static_analysis_found}
    end
  end
end
